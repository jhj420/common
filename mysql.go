package common

import "github.com/micro/go-micro/v2/config"

type MysqlConfig struct {
	Host string `json:"host"`
	User string `json:"user"`
	Pwd string `json:"pwd"`
	Database string `json:"database"`
	Port int64 `json:"port"`
}

//获取mysql的配置
func GetMysqlFromConsul(config config.Config,path ...string) *MysqlConfig  {
	mysqlConfig := &MysqlConfig{}
	//从path中读取配置放到mysqlConfig中
	config.Get(path...).Scan(mysqlConfig)
	return mysqlConfig
}